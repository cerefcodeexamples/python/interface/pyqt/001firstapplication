from datetime import datetime
from typing import Optional

from PyQt6.QtCore import Qt, QTimer
from PyQt6.QtWidgets import QLabel, QWidget

class ClockWidget(QLabel):

    def __init__(self, parent: Optional[QWidget] = None, 
            flags: Qt.WindowType = Qt.WindowType.Widget) -> None:
        
        super().__init__(
            datetime.now().strftime("%d/%m/%Y, %H:%M:%S"), 
            parent, 
            flags
        )

        # Init timer
        self.__init_timer()

    def __init_timer(self):
        # Init timer object
        self.__timer = QTimer(parent = self)

        # Define interval to 1s (in ms)
        self.__timer.setInterval(1000)

        # Connect interval time out event to update function
        self.__timer.timeout.connect(self.__update_current_time)

        # Start timer
        self.__timer.start()

    def __update_current_time(self):
        # Update label
        self.setText(datetime.now().strftime("%d/%m/%Y, %H:%M:%S"))