from typing import Optional

from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QMainWindow, QWidget

from src.widgets.clockwidget import ClockWidget

class MainWindow(QMainWindow):

    def __init__(self, parent: Optional[QWidget] = None, \
            flags: Qt.WindowType = Qt.WindowType.Window) -> None:
        super().__init__(parent, flags)

        # Init UI
        self.__init_ui()

    def __init_ui(self):
        # Init window size
        self.resize(640, 480)

        # Init clock widget
        self.__clock_widget = ClockWidget()

        # Set clock widget as central widget
        self.setCentralWidget(self.__clock_widget)
