# 001 - First application
This is an example to build your first application. For this example, we build
a clock application that displays the current date and hour.

## Requirements
This example requires the following Python packages:
- `PyQt6`, use the command `pip install PyQt6` to install this package.

## Summary
* [1. First empty window](#1-first-empty-window)
    * [1.1. Prepare the application](#11-prepare-the-application)
    * [1.2. Build the `MainWindow` class](#12-build-the-mainwindow-class)
    * [1.3. Run your first application](#13-run-your-first-application)
* [2. Building clock application](#2-building-clock-application)
    * [2.1. Init clock widget class](#21-init-clock-widget-class)
    * [2.2. Set the clock widget as main window central widget](#22-set-the-clock-widget-as-main-window-central-widget)
    * [2.3. Adding a Label widget to display some text](#23-adding-a-label-widget-to-display-some-text)
    * [2.4. Get the current date & hour](#24-get-the-current-date-hour)
    * [2.5. Adding an automatic update](#25-adding-an-automatic-update)

## 1. First empty window

### 1.1. Prepare the application
To build your first own application, first of all, init your application:
```python
if __name__ == "__main__":
    import sys
    from PyQt6.QtWidgets import QApplication

    # Init app
    app = QApplication(sys.argv)

    # Here will be the code for the main window
    ...

    # Start the event loop.
    app.exec()
```

### 1.2. Build the `MainWindow` class
Then you can prepare your `MainWindow` class (it's better to do it in a separate
file). In our case, we do it in the file `src/mainwindow.py`. The class must
inherit from `QMainWindow` class of the `PyQt6.QtWidgets` module:
```python
from typing import Optional

from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QMainWindow, QWidget

class MainWindow(QMainWindow):

    def __init__(self, parent: Optional[QWidget] = None, \
            flags: Qt.WindowType = Qt.WindowType.Window) -> None:
        super().__init__(parent, flags)

```

### 1.3. Run your first application
At this point, we can prepare the application and we have built a `MainWindow`
class. Now, we will launch our first application with an empty window. To do
that, we must init a `mainWindow` object after initialized the `app` object and
then display the main window with the `show()` method:
```python
if __name__ == "__main__":
    import sys
    from PyQt6.QtWidgets import QApplication

    from src.mainwindow import MainWindow

    # Init app
    app = QApplication(sys.argv)

    # Create a Qt widget, which will be our window.
    window = MainWindow()
    window.show()

    # Start the event loop.
    app.exec()
```

If you run this code, a little and empty window with be displayed. You can
change the default size withe the method `resize(width, height)` before
displaying the window.
```python
if __name__ == "__main__":
    import sys
    from PyQt6.QtWidgets import QApplication

    from src.mainwindow import MainWindow

    # Init app
    app = QApplication(sys.argv)

    # Create a Qt widget, which will be our window.
    window = MainWindow()

    # Change the default size
    window.resize(640, 480)

    # Display the window
    window.show()

    # Start the event loop.
    app.exec()
```

I recommend to integrate this default parameter directly in the `MainWindow`
class, so we can remove it from the `main.py` file: 
```python
from typing import Optional

from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QMainWindow, QWidget

class MainWindow(QMainWindow):

    def __init__(self, parent: Optional[QWidget] = None, \
            flags: Qt.WindowType = Qt.WindowType.Window) -> None:
        super().__init__(parent, flags)

        # Changing the default window size
        self.resize(640, 480)
```

## 2. Building clock application
From the code we have built until now we will add widgets to display the current
hour. There are two methods to build the clock:

- The first one is adding different widgets directly to the `MainWindow` class
in the constructor or a dedicated method. 

- The second one is building a dedicated widget that integrates all clock
features and then init this widget in the `MainWindow` class.

The first method can be used for small applications like this one we are
building but as the application growths the code will be more complex to handle.
So the second method is recommended and this tutorial will only implement the
second method.

### 2.1. Init clock widget class
Init a `ClockWidget` class in a different file (`src/widgets/clockwidget.py` for
this example). The class must inherit from `QWidget` of the module
`PyQt6.QtWidgets`:
```python
from typing import Optional

from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QWidget

class ClockWidget(QWidget):

    def __init__(self, parent: Optional[QWidget] = None, \
            flags: Qt.WindowType = Qt.WindowType.Widget) -> None:
        super().__init__(parent, flags)

```

### 2.2. Set the clock widget as main window central widget
In this case our application contains only one widget, so we will set it as
central application. But in more complex application, the central widget must be
a widget that will contains all other widgets but this case will be handled in
other tutorials.
```python
from typing import Optional

from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QMainWindow, QWidget

from src.widgets.clockwidget import ClockWidget

class MainWindow(QMainWindow):

    def __init__(self, parent: Optional[QWidget] = None, \
            flags: Qt.WindowType = Qt.WindowType.Window) -> None:
        super().__init__(parent, flags)

        # Init UI
        self.__init_ui()

    def __init_ui(self):
        # Init window size
        self.resize(640, 480)

        # Init clock widget
        self.__clock_widget = ClockWidget()

        # Set clock widget as central widget
        self.setCentralWidget(self.__clock_widget)
```

### 2.3. Adding a Label widget to display some text
The clock widget display a text which is a formatted text with the date and 
hour. So we will use a widget that is able to display text, the `QLabel` from
the module `PyQt6.QtWidgets`. So we will init a `QLabel` object with two
parameters, the first one will be the text `"Default text"` to check that the
label is correctly added to the clock widget. And the second one will be the
`parent` parameter set with the `self` value:

```python
from typing import Optional

from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QLabel, QWidget

class ClockWidget(QWidget):

    def __init__(self, parent: Optional[QWidget] = None, \
            flags: Qt.WindowType = Qt.WindowType.Widget) -> None:
        super().__init__(parent, flags)

        # Init ui
        self.__init_ui()

    def __init_ui(self):
        # Init label
        self.__label_widget = QLabel("Default text", parent=self)
```

At this state, if you run the application you must see a window that display the
text "Default text"

### 2.4. Get the current date & hour
To get the current date & hour, you can use the `datetime.now()` method from
the module `datetime`:
```python
from datetime import datetime

current_datetime = datetime.now()
```

`datetime.now()` returns a `datetime` object which is not directly compatible
with the `QLabel` which requires a string. To format the date & hour of the
`datetime` object, we will use the `strftime(format)` method. Here is the format
directives used in this example (but there are more available directives
[here](https://www.programiz.com/python-programming/datetime/strftime#format-code)):

| Directive | Meaning                                               | Example         |
|-----------|-------------------------------------------------------|-----------------|
| %d        | Day of the month as a zero-padded decimal.            | 01, 02, ..., 31 |
| %m        | Month as a zero-padded decimal number.                | 01, 02, ..., 12 |
| %Y        | Year with century as a decimal number.                | 2019, 2020...   |
| %H        | Hour (24-hour clock) as a zero-padded decimal number. | 00, 01, ..., 23 |
| %M        | Minute as a zero-padded decimal number.               | 00, 01, ..., 59 |
| %S        | Second as a zero-padded decimal number.               | 00, 01, ..., 59 |

```python
from datetime import datetime

current_datetime = datetime.now()

print(current_time.strftime("%d/%m/%Y, %H:%M:%S"))
# Output: 11/03/2024, 14:31:27
```

So, in the `ClockWidget` it will be implemented as follow:
```python
from datetime import datetime
from typing import Optional

from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QLabel, QWidget

class ClockWidget(QWidget):

    def __init__(self, parent: Optional[QWidget] = None, \
            flags: Qt.WindowType = Qt.WindowType.Widget) -> None:
        super().__init__(parent, flags)

        # Get current datetime
        self.__current_datetime = datetime.now()

        # Init ui
        self.__init_ui()

    def __init_ui(self):
        # Init label
        self.__label_widget = QLabel(
            self.__current_datetime.strftime("%d/%m/%Y, %H:%M:%S"),
            parent=self
        )
```

### 2.5. Adding an automatic update
This part will use a Qt logic that must be explained. There is Signal → Slot
relation. A signal can be connected to one or more slots and each time a signal
is emitted all the slots are executed. 

Example: `QPushButton` has the signal `pressed` which is executed when a button
is pressed. If we connect a slot, this code will be executed each time the
button is pressed.

In our case, we will use the `QTimer` class of the module `PyQt6.QtCore` which
can emit a signal after a defined time or periodically with the interval set. 
For this example, we want the label to be updated each second. So we will init a
`QTimer` object:
```python
from PyQt6.QtCore import QTimer

# Init my timer object
my_timer = QTimer(parent=...)
```

Then, we will define an interval of 1000 ms with the method
`setInterval(interval_in_ms)`:
```python
...
# Set interval of 1s (1000 ms)
my_timer.setInterval(1000) # in ms
...
```

The next step is create a slot and connect to the `timeout` signal which is
emitted at the end of an interval or at the end of the timer:
```python
def my_slot():
    # Do something
    ...

# Connect "timeout" signal to "my_slot" method
my_timer.timeout.connect(my_slot)
```

The last step is to start the timer with the method `start()`:
```python
...
# Start the timer
my_timer.start()
```

In the case of the `ClockWidget`, we integrate the `QTimer` object as a class
attribute to force the code to destroy the timer when the clock widget is 
destroyed and we make a method `__init_timer` to init the timer:
```python
from datetime import datetime
from typing import Optional

from PyQt6.QtCore import Qt, QTimer
from PyQt6.QtWidgets import QLabel, QWidget

class ClockWidget(QWidget):

    def __init__(self, parent: Optional[QWidget] = None, \
            flags: Qt.WindowType = Qt.WindowType.Widget) -> None:
        super().__init__(parent, flags)

        # Get current datetime
        self.__current_datetime = datetime.now()

        # Init ui
        self.__init_ui()

        # Init timer
        self.__init_timer()

    def __init_ui(self):
        # Init label
        self.__label_widget = QLabel(
            datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),
            parent=self
        )

    def __init_timer(self):
        # Init timer object
        self.__timer = QTimer(parent = self)

        # Define interval to 1s (in ms)
        self.__timer.setInterval(1000)

        # Connect interval time out event to update function
        self.__timer.timeout.connect(self.__update_current_time)

        # Start timer
        self.__timer.start()

    def __update_current_time(self):
        # Update label
        self.__label_widget.setText(datetime.now().strftime("%d/%m/%Y, %H:%M:%S"))
```

## 3. To go further
In this example, we have built a widget that integrate only one label widget but
we could optimize a bit the widget. Instead of building a widget that integrates
a label you could try to build a label widget. In practice, the class you will
build will inherit directly from `QLabel` instead of `QWidget` and you won't
need to init a label in the `__init_ui` method. The rest is pretty similar to
the class we already built instead that we don't update the label attribute but
directly the current object (`self`):
`self.__label_widget.setText(...)` → `self.setText(...)`

The `__init_ui` method won't be used anymore as we already initialize the label
in the constructor.

```python
from datetime import datetime
from typing import Optional

from PyQt6.QtCore import Qt, QTimer
from PyQt6.QtWidgets import QLabel, QWidget

class ClockWidget(QLabel):

    def __init__(self, parent: Optional[QWidget] = None, 
            flags: Qt.WindowType = Qt.WindowType.Widget) -> None:
        
        super().__init__(
            datetime.now().strftime("%d/%m/%Y, %H:%M:%S"), 
            parent, 
            flags
        )

        # Init timer
        self.__init_timer()

    def __init_timer(self):
        # Init timer object
        self.__timer = QTimer(parent = self)

        # Define interval to 1s (in ms)
        self.__timer.setInterval(1000)

        # Connect interval time out event to update function
        self.__timer.timeout.connect(self.__update_current_time)

        # Start timer
        self.__timer.start()

    def __update_current_time(self):
        # Update label
        self.setText(datetime.now().strftime("%d/%m/%Y, %H:%M:%S"))
```